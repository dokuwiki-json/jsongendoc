====== JSON Generate Document Example ======

This is example for the [[https://www.dokuwiki.org/plugin:jsongendoc|JSON Generate Document Plugin]].

===== Generate new Dokuwiki page =====
Page will be generated from [[template]] and filled with selected JSON data.

<code>
<jsongendoc path=data template=template docname=result_#### select='0:name, 1:type'>[
  {
    "name": "Caramel sauce",
    "type": "sweet sauce",
    "Ingredients": {
      "sugar": "200 g",
      "water": "2 spoons",
      "cream": "250 ml"
    }
  },
  {
    "name": "Ganache",
    "type": "sweet sauce",
    "Ingredients": {
      "chocolate": "250 g",
      "cream": "250 ml"
    }
  }
]</jsongendoc>
</code>
<jsongendoc path=data template=template docname=result_#### select='0:name, 1:type'>[
  {
    "name": "Caramel sauce",
    "type": "sweet sauce",
    "Ingredients": {
      "sugar": "200 g",
      "water": "2 spoons",
      "cream": "250 ml"
    }
  },
  {
    "name": "Ganache",
    "type": "sweet sauce",
    "Ingredients": {
      "chocolate": "250 g",
      "cream": "250 ml"
    }
  }
]</jsongendoc>

==== Result ====
  * [[result_caramel]]
  * [[result_ganache]]


===== Generate SVG document and download it =====
Document will be generated from [[template_svg]] and filled with selected JSON data. Data is already defined above.

<code>
<jsongendoc path=data mime=image/svg+xml template=template_svg docname=label.svg select='0:name, 1:type'>
</jsongendoc>
</code>
<jsongendoc path=data mime=image/svg+xml template=template_svg docname=label.svg select='0:name, 1:type'>
</jsongendoc>

After pressing a button svg document can be downloaded. It can be stored to the client machine or opened with specific application. It depends on the browser settings.

For example, svg document can be created with [[https://inkscape.org/|Inkscape]] and saved as svg file. File may already contain replacement patterns. Then text contents of the file can be just copied to dokuwiki template, which is referenced by ''<jsongendoc>''
