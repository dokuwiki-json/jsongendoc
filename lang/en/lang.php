<?php
/**
 * English language file for jsongendoc plugin
 *
 * @author Janez Paternoster <janez.paternoster@siol.net>
 */

$lang['add_button'] = 'Generate document';

$lang['err_json'] = 'No valid JSON data.';
$lang['err_no_page_name'] = 'Missing name for the new document.';
$lang['err_page_exists'] = 'Page %s allready exists.';
$lang['err_template_not_exist'] = 'Template %s does not exist.';
$lang['permision_denied_write'] = 'Permission Denied for write into %s.';
$lang['permision_denied_template'] = 'Permission Denied for read from template %s.';
$lang['err_empty_template'] = 'Template %s is empty, document %s is not created.';
$lang['err_tpl_section'] = 'Template %s: Error in pattern %s.';

$lang['err'] = 'ERROR';
$lang['file_created_summary'] = 'File created from template %s.';
$lang['file_created_msg'] = 'Document %s created from template %s.';
$lang['file_created_stat'] = 'Sections: %d shown, %d removed. Extracted: %d scalars, %d json, %d undefined (null).';
