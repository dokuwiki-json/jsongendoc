<?php
/**
 * Slovenian language file for jsongendoc plugin
 *
 * @author Janez Paternoster <janez.paternoster@siol.net>
 */

$lang['add_button'] = 'Ustvari dokument';

$lang['err_json'] = 'Ni veljavnih podatkov JSON.';
$lang['err_no_page_name'] = 'Manjka ime novega dokumenta.';
$lang['err_page_exists'] = 'Stran %s že obstaja.';
$lang['err_template_not_exist'] = 'Predloga %s ne obstaja.';
$lang['permision_denied_write'] = 'Ni dovoljenja za pisanje v %s.';
$lang['permision_denied_template'] = 'Ni dovoljenja za branje iz predloge %s.';
$lang['err_empty_template'] = 'Predloga %s je prazna, dokument %s ni ustvarjen.';
$lang['err_tpl_section'] = 'Predloga %s: Napaka v vzorcu %s.';

$lang['err'] = 'NAPAKA';
$lang['file_created_summary'] = 'Datoteka je ustvarjena iz predloge %s.';
$lang['file_created_msg'] = 'Dokument %s ustvarjen iz predloge %s.';
$lang['file_created_stat'] = 'Odseki: %d prikazanih, %d odstranjenih. Razširitve: %d vrednosti, %d json-ov, %d nedefiniranih (null).';
