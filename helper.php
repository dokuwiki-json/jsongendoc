<?php
/**
 * DokuWiki Plugin jsongendoc (Helper Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Janez Paternoster <janez.paternoster@siol.net>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) {
    die();
}

//Resolve to absolute page ID
use dokuwiki\File\PageResolver;

class helper_plugin_jsongendoc extends helper_plugin_json {

    /**
     * Handle extension to syntax_plugin_json_define
     *
     * @param array $data Reference to $data, which can be further manipulated
     */
    public function handle(&$data) {
        $json_o = $this->loadHelper('json');

        $mime = isset($data['keys']['mime']) ? $data['keys']['mime'] : '';
        $template = isset($data['keys']['template']) ? $data['keys']['template'] : '';
        $namespace = isset($data['keys']['namespace']) ? $data['keys']['namespace'] : '';
        $select = isset($data['keys']['select']) ? $data['keys']['select'] : '';
        $docname = isset($data['keys']['docname']) ? strval($data['keys']['docname']) : '';

        if($select) {
            $select = $json_o->parse_links($select);
        }

        if(!$namespace) {
            $namespace = getNS(getID());
        }

        //mime type for download or undefined for new Dokuwiki page
        $data['mime'] = $mime;
        //dokuwiki page ID for template (optional)
        $data['template'] = $template;
        //namespace for the new page
        $data['namespace'] = $namespace;
        //options for select dropdown (optional)
        //[name1=>[path,to,name1], name2=>[path,to,name2], ..., template=>[path,to,template]]
        $data['select'] = $select;
        //ID for the new page
        $data['docname'] = $docname;

        //If display is overriden, then show json tabs menu, othervise not.
        if(!isset($data['keys']['display'])) {
            $data['display'] = 'error';
        }
    }


    /**
     * Render extension to syntax_plugin_json_define
     *
     * For parameters see render::syntax_plugin_json_define
     *
     * @param array $data from handler
     * @param array $class definitions for parent div
     * @param array $data_attr html data attributes
     * @param array $tabs definitions for jQuery UI tabs widget
     * @param array $body definitions for jQuery UI tabs widget
     * @param array $log for reporting errors
     * @param array $tab_no must incerment for each tab
     * @param array $tab_number for id attribute
     */
    public function render(Doku_Renderer $renderer, &$data, &$class, &$data_attr, &$tabs, &$body, &$log, &$tab_no, $tab_number) {
        global $ID;

        $data_attr['active'] = $tab_no++;   //make this tab active

        //get (autoincrementing) filename and send it also to the javascript
        $docname = strval($data['docname']);
        $data_attr['docname'] = $docname;
        if(preg_match('/^(\S+?)(#+)$/', $docname, $matches)) {
            list(, $base, $hash) = $matches;
            $docname_meta = $base.str_replace('#', '0', $hash);
            // make absolute path
            $resolver = new PageResolver($data['namespace'].':page'); // ':page' will be stripped
            $docname_meta = $resolver->resolveId($docname_meta);
            //increment is stored in metadata
            $increment = p_get_metadata($ID, 'jsongendoc_'.$docname_meta);
            if(is_null($increment)) {
                $increment = 0;
            }
            //replace #### with zero padded $increment
            $docname = $base.str_pad(strval($increment), strlen($hash), "0", STR_PAD_LEFT);
        }
        else {
            $docname_meta = '';
        }

        //build select list with data integrated
        if(is_array($data['select']) && is_array($data['json_combined'])) {
            $options = array();
            foreach($data['json_combined'] as $select_data) {
                //get values from each select parameter
                $template = $data['template'];
                $names = array();
                foreach($data['select'] as $key => $path) {
                    $v = $select_data;
                    foreach($path as $tok) {
                        if(is_array($v) && isset($v[$tok])) {
                            $v = $v[$tok];
                        }
                        else {
                            $v = null;
                            break;
                        }
                    }
                    if(is_scalar($v)) {
                        if($key === 'template') {
                            $template = strval($v);
                        }
                        else {
                            $names[] = strval($v);
                        }
                    }
                }
                $name = htmlspecialchars(implode(' - ', $names));
                $v = htmlspecialchars(json_encode(array('parent_id'=>$ID, 'mime'=> $data['mime'], 'docname_meta'=> $docname_meta,
                    'template'=>$template, 'namespace'=>$data['namespace'], 'json'=>$select_data)));
                $options[] = "<option value=\"$v\">$name</option>";
            }
            $form = '<select>'.implode($options).'</select>';
        }
        else {
            $v = htmlspecialchars(json_encode(array('parent_id'=>$ID, 'mime'=> $data['mime'], 'docname_meta'=> $docname_meta,
                'template'=>$data['template'], 'namespace'=>$data['namespace'], 'json'=>$data['json_combined'])));
            $form = '<select hidden=""><option value="'.$v.'">-</option></select>';
        }

        //add name field and button to form
        $form .= '<input type="text" size="20" maxlength="255" value="'.htmlspecialchars($docname).'">';
        $form .= '<button>'.$this->getLang('add_button').'</button>';

        //jQuery-UI tabs
        $class[] = 'jsongendoc-plugin';
        $tabs[] = '<li><a href="#json-tab-'.$tab_number.'-table">Generate Document</a></li>';
        $body[] =      '<div id="json-tab-'.$tab_number.'-table"><div class="json-gendoc">'.$form.'</div><p class="json-gendoc-msg"></p></div>';
    }
}
