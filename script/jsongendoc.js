jQuery(function() {
    //Verify, if json-plugin is installed. Initialize it, if necessary.
    if(typeof json_plugin !== 'object') {
        return;
    }
    if(!json_plugin.initialized) {
        json_plugin.init();
    }

    jQuery('.jsongendoc-plugin').each(function() {

        var $tabs = jQuery(this),
            $tab = $tabs.find('.json-gendoc'),
            $msg = $tabs.find('.json-gendoc-msg'),
            $select = $tab.find('select'),
            $button = $tab.find('button'),
            $filename = $tab.find('input'),
            filenameTemplate = $tabs.data('docname');

        //button generate new document via ajax call
        $button.on('click', function (event) {
            jQuery.post(
                DOKU_BASE + 'lib/exe/ajax.php',
                {
                    call: 'jsongendoc_plugin',//server function
                    data: $select.val(), //data passed from helper.php
                    name: $filename.val()
                },
                function(data) {
                    $msg.append(data.msg);
                    if(data.mime.length > 0) {
                        //create file for download
                        var blob = new Blob([data.document], {type : data.mime});
                        //create link and click it for download to start
                        var link = document.createElement('a');
                        link.href = window.URL.createObjectURL(blob);
                        link.download = $filename.val();
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                    }
                    //increment filename field if necessary (for example,
                    //filenameTemplate is "P#####" and filename is "P00215")
                    var incLoc = filenameTemplate.search(/#+$/);
                    if (incLoc >= 0) {
                        var filename = $filename.val(),
                            incBase = filename.substring(0, incLoc),
                            inc = filename.substring(incLoc),
                            inc1 = (parseInt(inc) + 1).toString();
                        for (let i = inc.length - inc1.length; i > 0; i--) {
                            inc1 = "0" + inc1;
                        }
                        $filename.val(incBase + inc1);
                    }
                },
                'json'
            );
        });
    });

});
